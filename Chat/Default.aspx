﻿<%@ page language="C#" autoeventwireup="true" inherits="Chat_Default, App_Web_4p0w1hca" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel="shortcut icon" href="../Images/favicon.ico" type="image/x-icon" />
    <%=MainCss%>
    <link href="Icon/Ali2/iconfont.css" rel="stylesheet" />
    <script src="JS/jquery.min.js"></script>
    <script src="JS/mojocube.js?v=1"></script>

    <script>
        function initPage(url, type) {
            openUrl(url, 'ifLeft');
            openUrl('None.aspx', 'ifMain');

            switch (type)
            {
                case 0:
                    {
                        document.getElementById('i0').className = "icon iconfont syscolor";
                        document.getElementById('i1').className = "icon iconfont";
                        document.getElementById('i2').className = "icon iconfont";
                    }
                    break;
                case 1:
                    {
                        document.getElementById('i0').className = "icon iconfont";
                        document.getElementById('i1').className = "icon iconfont syscolor";
                        document.getElementById('i2').className = "icon iconfont";
                    }
                    break;
                case 2:
                    {
                        document.getElementById('i0').className = "icon iconfont";
                        document.getElementById('i1').className = "icon iconfont";
                        document.getElementById('i2').className = "icon iconfont syscolor";
                    }
                    break;
            }
        }
    </script>

</head>
<body style="padding:0px; margin:0px; overflow:hidden">
    <form id="form1" runat="server">
        
        <asp:ScriptManager id="ScriptManager1"  runat="server"></asp:ScriptManager>
    
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>

                <asp:TextBox ID="txtTitle" runat="server" Text="您有新的消息" Visible="false"></asp:TextBox>
        
                <asp:Timer id="Timer1" runat="server" Interval="1000" OnTick="Timer1_Tick">
                </asp:Timer>
        
            </ContentTemplate>
        </asp:UpdatePanel>

        <div style="width:60px; height:100%; background:#222D32; float:left">

            <div class="left_tx">
                <asp:Image ID="imgPortrait" runat="server" />
            </div>

            <div class="left_icon">
                <a href="javascript:;" onclick="initPage('ChatList.aspx',0)"><i id="i0" class="icon iconfont syscolor">&#xe779;</i></a>
            </div>
            
            <div class="left_icon">
                <a href="javascript:;" onclick="initPage('Contact.aspx',1)"><i id="i1" class="icon iconfont">&#xf0183;</i></a>
            </div>
            
            <div class="left_icon">
                <a href="javascript:;" onclick="initPage('Setting.aspx',2)"><i id="i2" class="icon iconfont">&#x3473;</i></a>
            </div>
            
            <div class="left_icon">
                <a href="Logout.aspx"><i class="icon iconfont">&#xf006f;</i></a>
            </div>

        </div>
        
        <div style="width:250px; height:100%; background:#E6E5E5; float:left">
            
            <iframe id="ifLeft" style="height:100%; width:100%; border:0px;" src="ChatList.aspx"></iframe>
            
        </div>
        
        <div style="height:100%; margin-left:310px;">

            <iframe id="ifMain" style="height:100%; width:100%; border:0px;" src="None.aspx"></iframe>
            
        </div>

    </form>
</body>
</html>
