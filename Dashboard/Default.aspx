﻿<%@ page language="C#" masterpagefile="~/Commons/Main.master" autoeventwireup="true" inherits="Dashboard_Default, App_Web_2nakqbkt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphMain" Runat="Server">

      <div class="content-wrapper">
      
        <section class="content-header">
          <h1>
            控制面板
          </h1>
          <ol class="breadcrumb">
            <li><a href="../"><i class="fa fa-home"></i> 首页</a></li>
            <li class="active">控制面板</li>
          </ol>
        </section>

        <section class="content">

        <div class="row">
            <div class="col-lg-3 col-xs-6">
              <!-- 考勤 -->
              <div class="small-box bg-aqua">
                <div id="AttendanceDiv" runat="server" class="inner"></div>
                <div class="icon">
                  <i class="fa fa-clock-o"></i>
                </div>
                <asp:HyperLink ID="hlAttendance" runat="server" CssClass="small-box-footer"></asp:HyperLink>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- 患者管理 -->
              <div class="small-box bg-green">
                <div id="DocumentDiv" runat="server" class="inner"></div>
                <div class="icon">
                  <i class="fa fa-user"></i>
                </div>
                <a href="../Customer/List.aspx?active=121,122" class="small-box-footer">更多 <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- 订单管理 -->
              <div class="small-box bg-red">
                <div id="ForumDiv" runat="server" class="inner"></div>
                <div class="icon">
                  <i class="fa fa-plus"></i>
                </div>
                <a href="../Order/List.aspx?active=143,144" class="small-box-footer">更多 <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
              <!-- 产品管理 -->
              <div class="small-box bg-yellow">
                <div id="AddressDiv" runat="server" class="inner"></div>
                <div class="icon">
                  <i class="fa fa-cube"></i>
                </div>
                <a href="../Product/List.aspx?active=130,131" class="small-box-footer">更多 <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
          </div>

        <div class="row">

        <section class="col-lg-7 connectedSortable">
            
            <!-- 公告通知 -->
            <div class="box">
                <div class="box-header">
                  <h3 class="box-title">公告通知</h3>
                  <div class="pull-right box-tools">
                    <div class="btn-group">
                      <button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i></button>
                      <ul class="dropdown-menu pull-right" role="menu">
                        <li><a href="../Notice/ViewList.aspx?active=34,37">公告通知</a></li>
                      </ul>
                    </div>
                    <button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-primary btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>

                <div id="NoticeDiv" runat="server" class="box-body table-responsive no-padding"></div>

             </div>
             
            <!-- 订单统计 -->
            <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">诊断统计</h3>
                  <div class="box-tools pull-right">
                      <button class="btn btn-primary btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="tab-content">
				    <div id="chart3" style="height:350px; margin:10px"></div>
                </div>
            </div>

            <!-- 流程管理 -->
            <div class="box hidden">
                <div class="box-header">
                  <h3 class="box-title">流程管理</h3>
                  <div class="pull-right box-tools">
                    <div class="btn-group">
                      <button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i></button>
                      <ul class="dropdown-menu pull-right" role="menu">
                        <li><a href="../Process/List.aspx?active=29,30">流程管理</a></li>
                        <li><a href="../Process/Edit.aspx?active=29,30">新增流程</a></li>
                      </ul>
                    </div>
                    <button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-primary btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>

                <div id="ProcessDiv" runat="server" class="box-body table-responsive no-padding"></div>

             </div>
             
            <!-- 工作计划 -->
            <div class="box hidden">
                <div class="box-header">
                  <h3 class="box-title">工作计划</h3>
                  <div class="pull-right box-tools">
                    <div class="btn-group">
                      <button class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i></button>
                      <ul class="dropdown-menu pull-right" role="menu">
                        <li><a href="../Plan/List.aspx?active=58,59">工作计划</a></li>
                        <li><a href="../Plan/Edit.aspx?active=58,59">新增计划</a></li>
                      </ul>
                    </div>
                    <button class="btn btn-primary btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-primary btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>

                <div id="PlanDiv" runat="server" class="box-body table-responsive no-padding"></div>

             </div>

        </section>

        <section class="col-lg-5 connectedSortable">
        
              <!-- 产品统计 -->
              <div class="box box-default hidden">
                <div class="box-header with-border">
                  <h3 class="box-title">订单产品</h3>
                  <div class="box-tools pull-right">
                      <button class="btn btn-primary btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="tab-content">
				    <div id="chart4" style="height:350px; margin:10px"></div>
                </div>
              </div>

              <div class="box box-solid bg-green-gradient">
                <div class="box-header">
                  <i class="fa fa-calendar"></i>
                  <h3 class="box-title">行事历</h3>
                  <div class="pull-right box-tools">
                    <div class="btn-group">
                      <button class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i></button>
                      <ul class="dropdown-menu pull-right" role="menu">
                        <li><a href="../Calendar/List.aspx?active=52,53">日程列表</a></li>
                        <li><a href="../Calendar/Edit.aspx?active=52,53">新增日程</a></li>
                        <li class="divider"></li>
                        <li><a href="../Calendar/Month.aspx?active=52,55">我的日历</a></li>
                      </ul>
                    </div>
                    <button class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <div class="box-body no-padding">
                 
                 <style>
                    .cal_header{ text-align:center;}
                    .cal_title a{ color:#ffffff !important; padding:0 10px;}
                    .cal_day{ position:relative;}
                 </style>
                    
                 <asp:Calendar ID="Calendar1" runat="server" BorderWidth="0px" DayNameFormat="Shortest" Font-Names="Arial" Font-Size="8pt" ForeColor="#404040" Height="240px" ShowGridLines="false" Width="100%" OnDayRender="Calendar1_DayRender">
                    <DayHeaderStyle BackColor="#00C86C" Font-Bold="True" Height="20px" BorderColor="#00C86C" ForeColor="#FFFFFF" HorizontalAlign="Center" CssClass="cal_header" />
                    <TitleStyle BackColor="#00C86C" BorderColor="Silver" Font-Bold="True" Font-Size="9pt" ForeColor="#FFFFFF" HorizontalAlign="Center" CssClass="cal_title" />
                    <DayStyle Font-Size="10pt" BorderColor="Silver" VerticalAlign="Middle" HorizontalAlign="Center" BackColor="#00C86C" ForeColor="#FFFFFF" CssClass="cal_day" />
                    <SelectedDayStyle BackColor="#00A65A" HorizontalAlign="Center" />
                    <TodayDayStyle BackColor="#005E33" HorizontalAlign="Center" />
                    <WeekendDayStyle ForeColor="#FFFFFF" BackColor="#00C86C" HorizontalAlign="Center" />
                    <OtherMonthDayStyle ForeColor="#005E33" HorizontalAlign="Center" />
                    <NextPrevStyle Font-Size="10pt" ForeColor="#3E75A4" Font-Bold="true" />
                 </asp:Calendar>

                </div>

              </div>
              
              <!-- 我的便签 -->
              <div class="box box-success">
                <div class="box-header">
                    <i class="fa fa-pencil"></i>
                    <h3 class="box-title">我的便签</h3>
                    <div class="pull-right box-tools">
                        <div class="btn-group">
                          <button class="btn btn-success btn-sm dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i></button>
                          <ul class="dropdown-menu pull-right" role="menu">
                            <li><a href="../User/Profile.aspx">我的便签</a></li>
                          </ul>
                        </div>
                        <button class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        <button class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body chat" id="chat-box">

                    <div id="MemoDiv" runat="server"></div>

                </div>
                <div class="box-footer">
                    <div class="input-group">
                    <asp:TextBox ID="txtMemoContent" runat="server" CssClass="form-control" placeholder="便签内容"></asp:TextBox>
                    <div class="input-group-btn">
                        <asp:LinkButton ID="lnbSaveMemo" runat="server" CssClass="btn btn-success" onclick="lnbSaveMemo_Click"><i class="fa fa-plus"></i></asp:LinkButton>
                    </div>
                    </div>
                </div>

               </div>

            </section>

        </div>

    </section>

    </div>
    
    <div id="ChartDiv" runat="server"></div>

</asp:Content>

