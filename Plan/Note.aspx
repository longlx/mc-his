﻿<%@ page language="C#" autoeventwireup="true" inherits="Plan_Note, App_Web_pzexqqc5" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
  <head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <link rel="shortcut icon" href="../Images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="../Skins/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../Css/font-awesome.min.css" />
    <link rel="stylesheet" href="../Css/ionicons.min.css" />
    <link rel="stylesheet" href="../Skins/dist/css/AdminLTE.min.css" />
    <%=skinCss%>
    <link rel="stylesheet" href="../Css/main.css" />
    <!--[if lt IE 9]>
        <script src="../JS/html5shiv.min.js"></script>
        <script src="../JS/respond.min.js"></script>
    <![endif]-->
    <script src="../JS/My97DatePicker/WdatePicker.js"></script>
    <script src="../JS/mojocube.js"></script>
  </head>

  <body class="hold-transition skin-<%=skin%> sidebar-mini">
  
     <form id="form1" runat="server">
        <div class="box-body">
        
              <div id="AlertDiv" runat="server"></div>

              <div class="row">
              
                  <div class="col-md-6 form-group">
                    <label><asp:Label ID="Label9" runat="server" Text="评价"></asp:Label></label>
                    <asp:TextBox ID="txtNote" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5"></asp:TextBox>
                  </div>
                  
                  <div class="box-footer">
                      <asp:Button ID="btnSave" runat="server" Text="保存" CssClass="btn btn-primary" onclick="btnSave_Click"></asp:Button>
                      <asp:Button ID="btnCancel" runat="server" Text="取消" CssClass="btn btn-default" onclick="btnCancel_Click"></asp:Button>
                  </div>

              </div>

        </div>
     </form>

    <script src="../Skins/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="../Skins/bootstrap/js/bootstrap.min.js"></script>
    <script src="../Skins/dist/js/app.min.js"></script>
    
    <script src="../Skins/plugins/iCheck/icheck.min.js"></script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-<%=skin%>',
                radioClass: 'iradio_flat-<%=skin%>',
                increaseArea: '20%'
            });
        });
    </script>

    <!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="../JS/Fancybox/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="../JS/Fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('.fancybox').fancybox();
            $(".loading-bg").hide();
        });
	</script>

  </body>
</html>
